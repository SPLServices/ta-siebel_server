.. CEF Add on for Splunk documentation master file, created by
   sphinx-quickstart on Sat Oct 13 11:00:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Siebel Server Add on for Splunk's documentation!
===========================================================

This add on implements the foundations for proper parsing for the product

Requirements
-----------------------------

This add on has index time extractions and must be installed on the indexer or heavy forwarder

- Splunk Enterprise 7.1 or newer
- Splunk Common Information Model 4.11 or newer


Installation
------------------------------

- Install the add on on each indexer and heavy forwarder
- Install the add on on each search head applicable
- Deploy the TA to UF
- Configure inputs to the appropriate UFs such as

.. code:: ini
   :name: inputs.conf

   [monitor:///SIEBEL_ROOT/siebsrvr/log/*.log]
   sourcetype=siebel:server
   [monitor:///SIEBEL_ROOT/siebsrvr/log/POP3SMTP_xxxxxxxxxxxxxxxx.log]
   sourcetype=siebel:pop3smtp

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
